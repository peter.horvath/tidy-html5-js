module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: "standard",
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module"
  },
  rules: {
    indent: ["error", 2],
    "linebreak-style": ["error", "unix"],
    quotes: ["error", "double"],
    "space-before-function-paren": ["error", "never"],
    "semi-style": ["error", "last"],
    semi: ["error", "always"],
    "space-infix-ops": ["error", { int32Hint: false }],
    curly: ["error", "all"]
  }
};
