import { readFile, readFileSync, readSync } from "node:fs";
import { dirname, normalize } from "node:path";
import { fileURLToPath } from "node:url";
import { randomFillSync, randomBytes } from "node:crypto";

// emscripten creates CommonJs require, so we need to implement a fake require()
const req = {
  fs: {
    readFile,
    readFileSync,
    readSync
  },
  path: {
    dirname,
    normalize
  },
  nodeUrl: {
    fileURLToPath
  },
  crypto: {
    randomFillSync,
    randomBytes
  }
};

// eslint-disable-next-line no-unused-vars
function require(str) {
  if (!(str in req)) {
    throw new Error("fake require problem");
  }
  return req[str];
}

const __filename = fileURLToPath(import.meta.url);
// eslint-disable-next-line no-unused-vars
const __dirname = dirname(__filename);

// eslint-disable-next-line
#include "dist/tidy"

let isInitialized = false;

let resolveInitPromise = null;

const initPromise = new Promise(function(resolve, reject) {
  resolveInitPromise = resolve;
});

// eslint-disable-next-line no-undef
Module.onRuntimeInitialized = function() {
  isInitialized = true;
  resolveInitPromise();
};

// eslint-disable-next-line no-undef
run();

function initialize() {
  if (isInitialized) {
    return Promise.resolve();
  } else {
    return initPromise;
  }
}

function execute(...args) {
  // eslint-disable-next-line no-undef
  return callMain(args);
}

export const Tidy = {
  initialize,
  execute
};
