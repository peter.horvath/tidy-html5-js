Fork of https://github.com/htacg/tidy-html5.git .

It compiles by emscripten into javascript, then it ismdistributed as an npm package on npmjs.com .

Usage scenarios:

1. command line, directly

Use either ./node_modules/.bin/tidy-js ...args...

or, if you installed it globally, then

tidy-js ...args...

2. command line, by npx:

npx tidy-js ...args...

It works both in local and in global installs.

3. From JavaScript, it is more complex. The most important problem is that it initializes asynchronously. It is up to you to solve its synchronous initialization. Good luck - it is both a strength and weakness of js. If you think you just write some function which will do it on a synchronous way, probably with some promise/await magic, you can safely forget that. To my best knowledge, it just can not happen and so is it.

We provide an ESM module. If you like the old, require way, again good luck. (Actually, I don't believe that ESM module would be a good step forward. But I also don't believe that require(), so CommonJs had been good. They can not import parallel, the yet more old AMD modules could, but their syntax was really weird.)

If you show some effort for me to give an ES6 version, I think I will probably do that. Particularly if you are some boss or tech lead somewhere, and you show interest to my CV :-)

So, the module initialization must run asynchronously, and then the calls are synchronous. Broargh, yes I fully understand you. I would be very happy over pull requests.

An example code:

```
import { Tidy } from 'tidy-js';

Tidy.initialize().then(
  Tidy.execute("--help");
  Tidy.execute("--asxml", "ex.html", "ex.xml");
);
```

So you can give `Tidy.execute` exactly the same parameters as you would give to the tidy command line tool, as command line arguments. To learn about them, google for "man tidy".

Note: **by default, `Tidy.execute(...)` is using the stdin, stdout and stderr of the calling process!** So, if you are using it in some special context, like in an express.js controller, you need to change things a little bit. We could do that quite easily to use strings, unfortunately as explained here https://stackoverflow.com/questions/32912129/providing-stdin-to-an-emscripten-html-program . Unfortunately, streams would not be really easy, at least not for input. Possibly it would be still possible by some digging into the emscripten API.

### Why I did not use the libtidy for that?

Because it is too low level. It was enough hard for us to learn the tidy command line, now learning the libtidy would be yet another pain. Beside that, libtidy is a really, really C thing, with pointers to structs and so. It would be painful to use from Js, it would be a dark forest what I do not wanted to enter. But again, if you send a pull request, odds are very high that you will get a greeting and a merge.
