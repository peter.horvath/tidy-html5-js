import { Tidy } from './dist/tidy.js';

await Tidy.initialize();

Tidy.execute(process.argv);
