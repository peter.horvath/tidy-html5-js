#!/usr/bin/make -f
.PHONY: default
default: dist/tidy.js dist/tidy-cli.js

.PHONY: test_deps
test_deps:
	which emcc
	which em++
	which make
	which cmake
	which xsltproc
	which touch
	which npx
	which node
	@#which m4
	which cpp

.PHONY: test
test: test_deps test.js dist/tidy.js
	node ./test.js

dist/.keep:
	[ -d dist ] || mkdir dist
	touch dist/.keep

EM_CC_FLAGS=-sENVIRONMENT=node,worker -sSUPPORT_BIG_ENDIAN=1 -sALLOW_MEMORY_GROWTH=1
EM_LINK_FLAGS=$(EMCCFLAGS) -sWASM=0 -sEXPORT_ES6=0 -sMALLOC=emmalloc -sINVOKE_RUN=0

dist/Makefile: dist/.keep
	cd dist && CC=emcc CXX=em++ cmake .. -DCMAKE_C_FLAGS="$(EM_CC_FLAGS)" -DCMAKE_EXE_LINKER_FLAGS="$(EM_LINK_FLAGS)"

dist/tidy: dist/Makefile
	rm -vf $@
	cd dist && make -j tidy

package-lock.json:
	npm install --include=dev

#dist/tidy_orig.js: tidy.m4.js dist/tidy
#	m4 - <$< >$@

dist/tidy_orig.js: tidy.cpp.js dist/tidy
	cpp -nostdinc -P <$< >$@

dist/tidy.js: dist/tidy_orig.js package-lock.json
	rm -vf $@
	node ./node_modules/.bin/js-beautify --config ./js-beautify.conf --file $< --outfile $@

.PHONY: pack
pack: dist/tidy.js dist/tidy-cli.js
	npm pack --pack-destination dist

dist/tidy_min.js: dist/tidy.js
	npx google-closure-compiler --module_resolution=node --js=$< --js_output_file=$@

dist/tidy-cli.js: tidy-cli.js
	cp -vfa $< $@

.PHONY: lint
lint:
	./node_modules/.bin/eslint --config eslintrc.cjs --fix *.js eslintrc.cjs

.PHONY: clean
clean:
	rm -rvf dist

.PHONY: publish
publish:
	npm publish --access public
