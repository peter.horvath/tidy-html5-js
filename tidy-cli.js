#!/usr/bin/env node
import { Tidy } from './dist/tidy.js';

Tidy.initialize().then(
  function() {
    Tidy.execute.apply(null, process.argv.slice(2));
  }
);
