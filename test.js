import { Tidy } from './dist/tidy.js';

await Tidy.initialize();

Tidy.execute("--help");

/*
Tidy.initialize().then(function() {
  Tidy.execute("--help");
  Tidy.execute("t.html");
});
*/
